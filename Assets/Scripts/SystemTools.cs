﻿using System.Collections;
using UnityEngine;
using System.Text.RegularExpressions;
using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text;
using System.Net;
public class SystemTools : MonoBehaviour {
		#region Params
		public static event Action timeComparedEvent;
		public static readonly string TIME_OK = "true";
		public static readonly string TIME_DOES_NOT_MATCH = "false";
		public static readonly string TIME_WAIT = "wait";
		private static string timeOk = TIME_WAIT;
		public static string TimeOk{
			get { return timeOk;}
			private set {timeOk = value;}
		}
		#region Singletone
		private static SystemTools _instance = null;
		public static SystemTools  Instance {
			get { return _instance;}
		}
		#endregion
		#region Toast
		private static AndroidJavaObject currentActivity;
		private static string toastMassage;
		private static bool socketConnect;
		#endregion
		#region Time
			private int threadSleepTime = 1000;
			Socket socket;
			public string[] ntpServer ;
			byte [] ntpData = new byte[48];
			Thread socketThread,restThread;
			public static DateTime networkTime;
			public static DateTime restNetworkTime;
			private bool _applicationPause;
			private event Action connetctingError;

			public static System.Func <bool> t_Predicate;
			public static bool restRequest;
			#if UNITY_EDITOR
			static bool unityPlayMode;
			#endif	
		#endregion
	#endregion
	void Awake (){
		if (_instance == null) {
			_instance = this;
			DontDestroyOnLoad (_instance);
		} else {
			Destroy (this);
		}
		ntpServer = new string [5] {"time-a.nist.gov", "time.windows.com", "ntp1.stratum2.ru"
			,"0.ua.pool.ntp.org","1.ua.pool.ntp.org"};
		socketThread = new Thread (Connect);
		restThread = new Thread (RestConnect);
//		BackgroundConnect ();
		PreferenceEncrypt.keys = new string[5];
		PreferenceEncrypt.keys [0] = "aegrf;o";
		PreferenceEncrypt.keys [1] = "qv34;9n";
		PreferenceEncrypt.keys [2] = "w2v3qa'";
		PreferenceEncrypt.keys [3] = "e4b697n";
		PreferenceEncrypt.keys [4] = "e35vw81";
		}
	
	void OnApplicationPause(bool pause){
		if (pause) {
			Debug.Log ("OnPause");
			_applicationPause = true;
		} else {
			Debug.Log ("OnResume");
			_applicationPause = false;
			BackgroundConnect ();
		}
	}
	void FixedUpdate(){
		#if UNITY_EDITOR
		unityPlayMode = UnityEditor.EditorApplication.isPlayingOrWillChangePlaymode;
		#endif
	}
	private void OnDestroy()
	{
		#if UNITY_EDITOR
			unityPlayMode = false;
		#endif
	}
	public static bool IsOnline (){
			bool connected;
		if (!Debug.isDebugBuild) {
			switch (Application.internetReachability) {
			case NetworkReachability.ReachableViaCarrierDataNetwork:
			case NetworkReachability.ReachableViaLocalAreaNetwork:
				connected = true;
				break;
			case NetworkReachability.NotReachable:
				connected = false;
				break;

			default:
				connected = false;
				break;
			}
		} else {
			connected = true;
		}
			return connected;
		}
	#region EmailCheck
		public static readonly string MatchEmailPattern =
			@"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
				+ @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
				  + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
				+ @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";
		public static readonly string MatchEtheriumPattern = @"^[a-km-zA-HJ-NP-Z0-9-]{5,}$";
		public static bool IsEmail(string email)
		{
			if (email != null)
				return Regex.IsMatch (email, MatchEtheriumPattern);
			else
				return false;
		}
		#endregion
	#region Toast
	public static void ShowToast(string toastString){

		#if UNITY_ANDROID && !UNITY_EDITOR
			AndroidJavaClass UnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
			currentActivity = UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
			toastMassage = toastString;
			currentActivity.Call ("runOnUiThread", new AndroidJavaRunnable (showToast));
		#elif UNITY_EDITOR
		DebugMessage (toastString);
		#endif
		}
	private static void showToast(){
			AndroidJavaObject context = currentActivity.Call<AndroidJavaObject>("getApplicationContext");
			AndroidJavaClass Toast = new AndroidJavaClass("android.widget.Toast");
			AndroidJavaClass Gravity = new AndroidJavaClass ("android.view.Gravity");
			AndroidJavaObject javaString = new AndroidJavaObject("java.lang.String",toastMassage);

			AndroidJavaObject toast = Toast.CallStatic<AndroidJavaObject> ("makeText", context, javaString, Toast.GetStatic<int> ("LENGTH_SHORT"));
			toast.Call ("setGravity", Gravity.GetStatic<int>("BOTTOM"),0,Screen.height/6);
			toast.Call ("show");
		}
	#endregion
	#region NetworkTime
	public void TimeRest(){
		if (restThread.ThreadState == ThreadState.Unstarted) {
			restThread.Start ();
			DebugMessage ("TimeRest->if");
		}else{
			if (!restThread.IsAlive){
				restThread = new Thread (RestConnect);
				restThread.Start ();
				DebugMessage ("TimeRest->else");
			}
		}
	}
	public void BackgroundConnect(){
		if (socketThread.ThreadState == ThreadState.Unstarted) {
			socketThread.Start ();
		}else{
			if (!socketThread.IsAlive){
				socketThread = new Thread (Connect);
				socketThread.Start ();
			}
		}
	}

	public void SocketErrorConnection(){
		Debug.Log ("Socket exception start!");
		socketThread.Abort ();
		socketThread.Join ();
		Debug.Log (socketThread.ThreadState);
		BackgroundConnect ();
		Debug.Log ("SocketException ending!");
	}

	public void RestConnect(){
		System.Random r = new System.Random ();
		bool result = false;
		int tempServer = r.Next (0, ntpServer.Length);


		ntpData [0] = 0x1B;
		try {
			var addresses = Dns.GetHostEntry (ntpServer[tempServer]).AddressList;

			var ipEndPoint = new IPEndPoint (addresses [0], 123);
			socket = new Socket (AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
			socket.Connect (ipEndPoint);
			socket.ReceiveTimeout = 3000;
			SendRequest (out networkTime);
			Debug.Log ("RestThread Done! Time is now: "+ networkTime
				+ ". Time taken from sever: " + ntpServer[tempServer]);
			result = true;
		}catch(SocketException socketEx){
			Debug.Log ("RestThreadException: " + socketEx.ToString());
			result = false;
		}
		catch (Exception ex) {
			Debug.LogException (new Exception ("Time Comparer exception: " + ex.ToString()));
			socket.Close ();
			connetctingError.Invoke ();
			Debug.Log ("RestConnect -> Exception: " + ex.ToString());
			result = false;
		}
		restRequest = result;
		Debug.Log ("RestThread: WorkDone!");
	}

	public void Connect (){
		while (!_applicationPause) {
			System.Random r = new System.Random ();

			int tempServer = r.Next (0, ntpServer.Length);

			//default Windows time server

			// NTP message size - 16 bytes of the digest (RFC 2030)

			//Setting the Leap Indicator, Version Number and Mode values
			ntpData [0] = 0x1B; //LI = 0 (no warning), VN = 3 (IPv4 only), Mode = 3 (Client Mode)
			try {
				var addresses = Dns.GetHostEntry (ntpServer[tempServer]).AddressList;

				//The UDP port number assigned to NTP is 123
				var ipEndPoint = new IPEndPoint (addresses [0], 123);
				//NTP uses UDP
				socket = new Socket (AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
				socket.Connect (ipEndPoint);
				socket.ReceiveTimeout = 3000;
				Debug.Log ("Connect -> try");
				SendRequest (out networkTime);
				Debug.Log ("Network time parsing done! Time is now: "+ networkTime
					+ ". Time taken from sever: " + ntpServer[tempServer]);
			}catch(SocketException socketEx){
				Debug.Log ("SocketException: " + socketEx.ToString());
			}
			catch (Exception ex) {
				Debug.LogException (new Exception ("Time Comparer exception: " + ex.ToString()));
				socket.Close ();
				connetctingError.Invoke ();
				Debug.Log ("TimeComparer-> after socketThread.Join");
			}
			
			#if !UNITY_EDITOR && UNITY_ANDROID
			if (!_applicationPause) {
			Debug.Log ("TimeComperer go to sleep");
			Thread.Sleep (threadSleepTime);
			} else {
			Debug.Log ("Time thread closing normaly before return");
			return;
			Debug.Log ("Time thread closing normaly after return");
			}
			#elif UNITY_EDITOR
			Debug.Log ("InUnityEditor #if");
			if (unityPlayMode){
				if (!_applicationPause) {
					Debug.Log ("TimeComperer go to sleep");
					Thread.Sleep (threadSleepTime);
				} else {
					Debug.Log ("Time thread closing normaly before return");
					return;
					Debug.Log ("Time thread closing normaly after return");
				}
			}else {
				Debug.Log("Main time comparer thread dead...");
				return;
			}
			#endif
		}
	}

	public void SendRequest(out DateTime time){

		socket.Send (ntpData);

		socket.Receive (ntpData);

		const byte serverReplyTime = 40;

		//Get the seconds part
		ulong intPart = BitConverter.ToUInt32(ntpData, serverReplyTime);

		//Get the seconds fraction
		ulong fractPart = BitConverter.ToUInt32(ntpData, serverReplyTime + 4);

		//Convert From big-endian to little-endian
		intPart = SwapEndianness(intPart);
		fractPart = SwapEndianness(fractPart);
		var milliseconds = (intPart * 1000) + ((fractPart * 1000) / 0x100000000L);

		//**UTC** time
		var networkDateTime = (new DateTime(1900, 1, 1, 0, 0, 0, DateTimeKind.Utc)).AddMilliseconds((long)milliseconds);
		time = networkDateTime.ToLocalTime ();
		//		return networkDateTime.ToLocalTime();
	}

	static uint SwapEndianness(ulong x)
	{
		return (uint) (((x & 0x000000ff) << 24) +
			((x & 0x0000ff00) << 8) +
			((x & 0x00ff0000) >> 8) +
			((x & 0xff000000) >> 24));
	}

	public void CloseSocket (){
		socket.Close ();
	}

	public void ShowTime(){
		//		DateTime dt = SendRequest ();
		//		Debug.Log (dt);
		string str =  networkTime.ToString();
		Debug.Log (networkTime);
		#if UNITY_ANDROID && !UNITY_EDITOR
		ShowToast (str);
		#endif
	}

	public void ThreadLife(){
		Debug.Log ("TimeCompererThread life: " + socketThread.IsAlive);
		if (!socketThread.IsAlive){
			socketThread = new Thread (Connect);
			socketThread.Start ();
		}
	}
		#endregion

	public static void DebugMessage (object msg, bool b = false){
			if (b) {
				Debug.Log (msg);
			} else {
				if (Debug.isDebugBuild){
					Debug.Log (msg);
				}
			}
		}

	public void ApplicationQuit(float t){
		StartCoroutine (ShutDown(t));
	}
	private IEnumerator ShutDown(float t){
		yield return new WaitForSeconds (t);
		Application.Quit();
	}
}




