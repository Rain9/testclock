﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Clock : MonoBehaviour {
	#region Params
		private Text _clock;
	private Button _btn;
	#endregion
 
	void Start()
	{
		_btn = this.gameObject.GetComponentInChildren<Button>();
		_clock = this.gameObject.GetComponentInChildren<Text>();
		StartCoroutine(Timer());
		_btn.onClick.AddListener(() =>
		{
			SystemTools.ShowToast("this is custom toast!");
		});
	}

	

	IEnumerator Timer()
	{
		yield return new WaitWhile (() => SystemTools.networkTime == DateTime.MinValue);
		while (true)
		{
			Debug.Log(this.name +  SystemTools.networkTime);
			_clock.text = "Time is now: " + SystemTools.networkTime.ToString("T") + 
				"\n Date: " + SystemTools.networkTime.ToString("D");
			yield return new WaitForSeconds(1f);
		}
		
	}

}
